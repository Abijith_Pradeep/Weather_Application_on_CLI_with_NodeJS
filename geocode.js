const request = require('request')

const weather = require('./weather')

var geocodeAddress = (address) => {
    var encodedAddress = encodeURIComponent(address)
    request({
        url : `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}`,
        json : true
    }, (error, response, body) => { 
        if (error) {
            console.log('Unable to connect to Google servers')
        }
        else if (body.status === 'ZERO_RESULTS') {
            console.log("Unable to find the address")
        }
        else if (body.status === 'OK') {
            lat = body.results[0].geometry.location.lat
            lgn = body.results[0].geometry.location.lng
            console.log("_______________________\n")
            console.log(`Address : ${body.results[0].formatted_address}`)
            console.log(`Latitude : ${lat}`)
            console.log(`Longitude : ${lgn}`)   
            weather.weatherConditions(lat, lgn)
            }
        else {
            console.log(`Error occured : ${body.status}`)
        }
    })
};

module.exports.geocodeAddress = geocodeAddress