<h1>Weather Application</h1>
This is a **CLI application built with NodeJs**. It **provides the weather details along with the address of the area**, whose Pincode is passed.

<h3>Pre-requisites :</h3>

    1. Install NodeJS 
    2. Perfom " node install " command inside the cloned repository. This will automatically install all the required modules.

<h3>Usage pattern :</h3>

    1. node filename --address pincode 
       node filename --a pincode
    2. node filename --help
       node filename --h
 