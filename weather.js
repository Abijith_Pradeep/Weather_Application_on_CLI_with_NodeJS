const request = require('request')
var weatherConditions = (lat,lgn) => {
    var apiKey = 'e7fe70ea50fd0bfb30a3014d952a4698'
    request ({
        url : `https://api.darksky.net/forecast/${apiKey}/${lat},${lgn}`,
        json : true,
    }, (error, response, body) => {
        if (!error && response.statusCode == 200){
            console.log("_______________________\n")
            console.log (`Its currently ${body.currently.temperature} F but it seems like ${body.currently.apparentTemperature} F`)
        }else {
            console.log ('Unable to fetch weather.')
        }

    })
}

module.exports.weatherConditions = weatherConditions